# README #


### What is this repository for? ###

* This repository is a cucumber BDD UI and API testing TestNG framework using selenium and RESTAssured java libaries created for the Standards Australia coding interview
* 1.0


### How do I get set up? ###

* Dependencies: 
	i. The framework is designed to run tests in firefox browser hence firefox should be installed in the execution machine.
	ii. Maven should be configured in the path variables to execute the tests using maven command.
* How to run tests
	1. Open command prompt
	2. Enter the command 'mvn clean test'

