package standards.codechallenge.apidef;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.http.*;
import io.restassured.specification.RequestSpecification;
import java.util.*;
import standards.codechallenge.managers.*;

/**
 * API is the base class for all API definition classes which contains all the
 * common functions.
 */
public class RestAPI {

	RequestSpecification request;

	Response response;
	
	String apiURI;

	public RestAPI(String APIURI) {
		
		apiURI = FileReaderManager.getInstance().getConfigReader().properties.getProperty("apiEndpoint") + APIURI;
		
		Log.INFO(apiURI);
		createRequest();

	}
	
	public Response getResponse() {
		return response;
	}
	
	public String getapiURI() {
		return apiURI;
	}
	
	public void apiGet() {
		response = request.get(apiURI);
		
	}
	
	private void createRequest() {

		request = RestAssured.given();
		List<Header> list = new ArrayList<Header>();
		list.add(new Header("Host", "5M560VM"));
		list.add(new Header("User-Agent", "RestAssuredLibrary"));
		list.add(new Header("Connection", "keep-alive"));
		Headers header = new Headers(list);
		request.headers(header);
	}

}
