package standards.codechallenge.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import standards.codechallenge.managers.Log;

import java.time.Duration;
import java.time.LocalDateTime;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

/**
 * Base page initializes the webdriver and contains all the common functions which can be reused across page objects
 */

public class BasePage {
	
	public BasePage(WebDriver driver) {
		
		this.driver = driver;
		
	}
	

	
	protected WebDriver driver;
	
	/**
	 * Function to get a web element from a shadow DOM
	 * @param elementSelector selector string of the element that needs to be returned
	 * @return WebElement from the shadow DOM
	 */
	protected WebElement getShadowDOMWebElement(String shadowRootSelector,String elementSelector) {
		
		WebElement element = null;
		
		boolean stopLoop = false;
		
		LocalDateTime timeout = LocalDateTime.now().plusSeconds(30);

		while (!stopLoop) {
			
				
			try {
				
				element = (WebElement) ((JavascriptExecutor)driver).executeScript("return document.querySelector(\"" + shadowRootSelector + "\").shadowRoot.querySelector(\"" + elementSelector + "\")");
				Log.INFO(element.toString());
				stopLoop = true;
				
			}catch (Exception e) {
				if(timeout.isBefore(LocalDateTime.now())){
					stopLoop = true;
				}
			}
			
		}
		
		return element;
		
	}
	
	
	public void waitForObject(By selector) {
		
		(new WebDriverWait(driver, Duration.ofSeconds(30))).until(ExpectedConditions.visibilityOfElementLocated(selector));
		
	}

}
