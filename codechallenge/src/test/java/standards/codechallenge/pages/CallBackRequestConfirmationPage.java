package standards.codechallenge.pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CallBackRequestConfirmationPage extends BasePage {
	
	By thankYouHeadingSelector = By.cssSelector("#page-outcome-rowthankyoupageheader-columns3-thankyoupageheadercontent1 h3");
	
	CallBackRequestConfirmationPage(WebDriver driver) {
		super(driver);
	}
	
	public String getThankYouText(){
		(new WebDriverWait(driver,Duration.ofSeconds(30))).until(ExpectedConditions.visibilityOfElementLocated(thankYouHeadingSelector));
		return driver.findElement(thankYouHeadingSelector).getText();
	}

}
