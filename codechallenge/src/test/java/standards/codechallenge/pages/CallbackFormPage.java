package standards.codechallenge.pages;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class CallbackFormPage extends BasePage {
	
	By existingCustomerNoButtonSelector = By.xpath("//label[span='No']");
	By firstNameTextBoxSelector = By.id("field-page-Page1-aboutYou-firstName");
	By lastNameTextBoxSelector = By.id("field-page-Page1-aboutYou-lastName");
	By stateDropdownSelector = By.cssSelector("#page-Page1-aboutYou-state div.react-select__control");
	By phoneNumberTextBoxSelector = By.id("field-page-Page1-aboutYou-phoneNumber");
	By emailTextBoxSelector = By.id("field-page-Page1-aboutYou-email");

	public CallbackFormPage(WebDriver driver) {
		super(driver);
		(new WebDriverWait(driver,Duration.ofSeconds(30))).until(ExpectedConditions.visibilityOfElementLocated(By.id("Page1")));
	}

	public void clickExistingCustomerNoButton() {
		
		driver.findElement(existingCustomerNoButtonSelector).click();
		
	}
	
	public void setFirstName(String firstName) {
		
		driver.findElement(firstNameTextBoxSelector).sendKeys(firstName);
		
	}
	
	public void setLastName(String lastName) {
		
		driver.findElement(lastNameTextBoxSelector).sendKeys(lastName);
		
	}
	
	public void selectState(String state) {
		WebElement dropdown = driver.findElement(stateDropdownSelector);
		String statesPassed = "";
		String option = "";
		Actions act = new Actions(driver);
		do {
			
			act.moveToElement(dropdown).click().sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).build().perform();
			
			WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(1));
			try {
				wait.until(ExpectedConditions.textToBePresentInElementValue(stateDropdownSelector, "<>"));				
			}catch (Exception e) {
			}

			option = dropdown.getText();
			
			if(statesPassed.contains("<"+option+">")) {
				Assert.fail("Invalid test data for state");
			}
			
			statesPassed = statesPassed + "<" + option + ">";
			
		}while(!option.equalsIgnoreCase(state));

		
	}
	
	public void setPhoneNumber(String phoneNumber) {
		
		driver.findElement(phoneNumberTextBoxSelector).sendKeys(phoneNumber);
		
	}
	
	public void setEmail(String email) {
		
		driver.findElement(emailTextBoxSelector).sendKeys(email);
		
	}
	
	public CallBackRequestConfirmationPage clickSubmit() {
		
		WebElement emailTextBox = driver.findElement(emailTextBoxSelector);
		Actions act = new Actions(driver);
		act.moveToElement(emailTextBox).sendKeys(Keys.TAB).sendKeys(Keys.ENTER).build().perform();
		return new CallBackRequestConfirmationPage(driver);
	}

}
