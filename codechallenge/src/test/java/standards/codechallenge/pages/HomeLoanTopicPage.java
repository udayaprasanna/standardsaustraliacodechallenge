package standards.codechallenge.pages;


import org.openqa.selenium.WebDriver;

public class HomeLoanTopicPage extends BasePage {
	

	public HomeLoanTopicPage(WebDriver driver) {
		super(driver);
	}
	
	String shadownElementSelector = "div#contact-form-shadow-root";
	
	public void selecthomeLoanTopic(String homeLoanType){
		
		if(homeLoanType.equalsIgnoreCase("New home loans")) {

			getShadowDOMWebElement(shadownElementSelector, "#myRadioButtons-0 label span").click();			
			
		}
		
	}
	
	public NewHomeLoansTopicPage clickNext() {
		
		
		getShadowDOMWebElement(shadownElementSelector, "button").click();
		return new NewHomeLoansTopicPage(driver);
		
	}

}
