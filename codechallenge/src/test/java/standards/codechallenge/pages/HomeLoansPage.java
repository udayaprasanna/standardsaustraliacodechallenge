package standards.codechallenge.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomeLoansPage extends BasePage {
	
	By requestACallbackLinkSelector = By.xpath("//a[span='Request a call back']");

	public HomeLoansPage(WebDriver driver) {
		super(driver);
	}

	public HomeLoanTopicPage clickRequestACallbackLink() {
		
		waitForObject(requestACallbackLinkSelector);
		driver.findElement(requestACallbackLinkSelector).click();
		
		return new HomeLoanTopicPage(driver);
		
	}

}
