package standards.codechallenge.pages;


import org.openqa.selenium.*;


public class HomePage extends BasePage{
	
	By personalLinkSelector = By.xpath("//a[text()='Personal']");
	
	By homeLoanLinkSelector = By.cssSelector("ul.mega-menu a[href$='home-loans'].menu-trigger");
	
	By dropDownHomeLoanLinkSelector = By.cssSelector("ul.mega-menu ul.dropdown a[href$='home-loans']");
	
	public HomePage(WebDriver driver) {
		
		super(driver);
		
	}
	
	public void clickPersonalLink() {
		
		waitForObject(personalLinkSelector);
		
		driver.findElement(personalLinkSelector).click();
		
	}

	public void clickHomeLoanLink() {
		
		waitForObject(homeLoanLinkSelector);
		
		driver.findElement(homeLoanLinkSelector).click();
		
	}
	
	public HomeLoansPage clickDropDownHomeLoanLink() {
		
		waitForObject(dropDownHomeLoanLinkSelector);
		
		driver.findElement(dropDownHomeLoanLinkSelector).click();
		
		return new HomeLoansPage(driver);
		
	}
	

}
