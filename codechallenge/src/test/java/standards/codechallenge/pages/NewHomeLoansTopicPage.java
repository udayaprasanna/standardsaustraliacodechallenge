package standards.codechallenge.pages;


import java.util.ArrayList;
import org.openqa.selenium.WebDriver;

public class NewHomeLoansTopicPage extends BasePage {
	
	String shadownElementSelector = "div#contact-form-shadow-root";

	public NewHomeLoansTopicPage(WebDriver driver) {
		super(driver);
	}

	public void selectEnquiryType(String enquiryType) {
		
		if(enquiryType.equalsIgnoreCase("Buying a new property")) {

			getShadowDOMWebElement(shadownElementSelector, "#myRadioButtons-0 label span").click();			
			
		}
		
	}
	
	public CallbackFormPage clickNext(){
		
		
		getShadowDOMWebElement(shadownElementSelector, "button:last-child").click();
		
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		return new CallbackFormPage(driver);
		
	}

}
