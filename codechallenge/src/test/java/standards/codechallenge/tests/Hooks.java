package standards.codechallenge.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.*;
import standards.codechallenge.managers.Log;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import java.time.Duration;



public class Hooks {

	public Hooks(ObjectContainer container) {
		globalContainer = container;
	}

	private static ObjectContainer globalContainer;
	private static WebDriver globalDriver;

	@After
	public void afterTest(Scenario scenario) {
		if(!scenario.getName().toLowerCase().contains("api")) {
			globalDriver.quit();
		}
	}

	@Before
	public void beforeTest(Scenario scenario) {
		
		if(!scenario.getName().toLowerCase().contains("api")) {
			
			System.setProperty("webdriver.gecko.driver", "BrowserDrivers\\geckodriver.exe");
			globalDriver = new FirefoxDriver();
			globalDriver.manage().window().maximize();
			globalDriver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
			globalDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
			globalDriver.manage().deleteAllCookies();
			globalContainer.register("globalDriver", globalDriver);
			
		}
		Log.INFO("Start of scenario " + scenario.getName());
		

	}

}
