package standards.codechallenge.tests;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

	/**
	 * ObjectContainer is a simple Registry used to store and retrieve common objects by type or identifier. 
	 * Can be used with Dependency Injection for sharing dependencies.
	 */
	public class ObjectContainer {
		
		private static Map<String, Object> map;
		
		public ObjectContainer() {
			if(map == null) {
				map = new HashMap<String, Object>();
			}
		}
		
		/**
		 * Checks the entries for an entry with a matching type
		 * @param type the type you want to check
		 * @return true if a matching entry was found and false otherwise
		 */
		public boolean hasEntry(Type type) {
			return map.containsKey(type.getTypeName());
		}
		
		/**
		 * Registers an entry in the container. If the container already has 
		 * an entry of the same type it will be overwritten.
		 * @param entry the entry to store in the container
		 */
		public <T> void register(T entry) {
			map.put(entry.getClass().getTypeName(), entry);
		}
		
		/**
		 * Resolves an entry from the container with the matching type. null 
		 * will be returned if a matching entry cannot be found.
		 * @param type the type of the entry you wish to resolve
		 * @return the matching entry or null if a matching entry could not be found
		 */
		@SuppressWarnings("unchecked")
		public <T> T resolve(Type type) {
			return (T) map.get(type.getTypeName());
		}
		
		/**
		 * Registers an entry in the container with an entry name which allows 
		 * multiple entries of the same type to be registered. 
		 * @param entryName the unique name of the entry
		 * @param entry the entry to store in the container
		 */
		public <T> void register(String entryName, T entry) {
			map.put(entryName, entry);
		}
		
		/**
		 * Resolves an entry from the container with the matching name. null 
		 * will be returned if a matching entry cannot be found.
		 * @param entryName the name of the entry to you wish to resolve
		 * @return the matching entry or null if a matching entry could not be found
		 */
		@SuppressWarnings("unchecked")
		public <T> T resolve(String entryName) {
			return (T) map.get(entryName);
		}

		/**
		 * Removes an entry by type.
		 * @param type the entry type to remove
		 */
		public <T> void remove(Type type) {
			map.remove(type.getTypeName());
		}
		
		/**
		 * Removes an entry by name
		 * @param entryName the name of the entry to remove
		 */
		public void remove(String entryName) {
			map.remove(entryName);
		}
		
		/**
		 * Removes all entries
		 * 
		 */
		public void removeAll() {
			map.clear();
		}

		/**
		 * Registers an entry as a superclass or superinterface
		 * @param entry the entry to store in the container
		 * @param asType the type to register the entry as
		 */
		public <T> void registerAs(T entry, Class<?> asType) {
			if(!asType.isInstance(entry)) {
				throw new IllegalArgumentException("The entry must extend or implement the asType");
			}
			map.put(asType.getTypeName(), entry);
		}
		
	}

