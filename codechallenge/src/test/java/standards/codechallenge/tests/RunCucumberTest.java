package standards.codechallenge.tests;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = { "pretty", "html:target/cucumber-html-reports/cucumber.html",
        "json:target/cucumber-html-reports/cucumber.json"}, features = { "src/test/resources/features" })
public class RunCucumberTest extends AbstractTestNGCucumberTests {

}
