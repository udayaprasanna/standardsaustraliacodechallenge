package standards.codechallenge.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import io.cucumber.java.en.*;
import standards.codechallenge.apidef.RestAPI;
import standards.codechallenge.managers.FileReaderManager;
import standards.codechallenge.managers.Log;
import standards.codechallenge.pages.*;

public class Stepdefs {

	public Stepdefs(ObjectContainer container) {

		this.container = container;
		driver = container.resolve("globalDriver");
	}

	public ObjectContainer container;
	protected WebDriver driver;

	HomePage homePage;
	HomeLoansPage homeLoansPage;
	HomeLoanTopicPage homeLoanTopicPage;
	NewHomeLoansTopicPage newHomeLoansTopicPage;
	CallbackFormPage callbackFormPage;
	CallBackRequestConfirmationPage callBackRequestConfirmationPage;
	RestAPI weatherbitApi;

	@Given("^I am at the NAB home page$")
	public void i_am_at_the_nab_home_page() {

		driver.get(FileReaderManager.getInstance().getConfigReader().properties.getProperty("baseURL"));
		homePage = new HomePage(driver);

	}

	@When("^I enquire about (.*?) for (.*?)$")
	public void homeLoanEnquiry(String homeLoanType, String enquiryType) {

		homePage.clickPersonalLink();
		homePage.clickHomeLoanLink();
		homeLoansPage = homePage.clickDropDownHomeLoanLink();

		homeLoanTopicPage = homeLoansPage.clickRequestACallbackLink();

		homeLoanTopicPage.selecthomeLoanTopic(homeLoanType);
		newHomeLoansTopicPage = homeLoanTopicPage.clickNext();

		newHomeLoansTopicPage.selectEnquiryType(enquiryType);
		callbackFormPage = newHomeLoansTopicPage.clickNext();
		

		
	}
	
	@And("^I enter my (.*?), (.*?), (.*?), (.*?), (.*?) in the call back form and click submit$")
	public void fillCallbackForm(String firstName, String lastName, String state, String phoneNumber, String email) {
		
		callbackFormPage.clickExistingCustomerNoButton();
		callbackFormPage.setFirstName(firstName);
		callbackFormPage.setLastName(lastName);
		callbackFormPage.selectState(state);
		callbackFormPage.setPhoneNumber(phoneNumber);
		callbackFormPage.setEmail(email);
		callBackRequestConfirmationPage =  callbackFormPage.clickSubmit();

	}

	@Then("^the callback request should be accepted and thank you page with (.*?) should be displayed$")
	public void callbackRequestAcceptanceVerification(String firstName) {
		
		Assert.assertEquals("Thank you, " + firstName, callBackRequestConfirmationPage.getThankYouText());
		
	}
	
	@Given("^I have a weather forecast api that can fetch weather for (.*?) using weather forecast api request$")
	public void addTestDataToWeatherForecastAPIURI(String postCode) {
		
		weatherbitApi = new RestAPI("forecast/hourly?key=f1cbf6c7744444eb9b5a890801ec4ee8&postal_code="+postCode+"&lang=en&hours=3");

	}

	@When("^I send the request to weather forecast API$")
	@When("^I send the request to current weather API$")
	public void sendGetRequest() {
		
		weatherbitApi.apiGet();
	}

	@Then("^the the reponse validation for (.*?) should pass$")
	public void the_the_reponse_validation_for_data_state_code_switch_set_successfully_should_pass(String expectedResponseCode) {
		
		Log.INFO(weatherbitApi.getResponse().asString());
		Assert.assertEquals(String.valueOf(weatherbitApi.getResponse().getStatusCode()),expectedResponseCode);
		
		if(weatherbitApi.getapiURI().toLowerCase().contains("current")) {
			
			Log.INFO("/data/state_code : " + weatherbitApi.getResponse().jsonPath().get("data[0].state_code"));
			
		}else {
			Log.INFO("timestamp_utc : " + weatherbitApi.getResponse().jsonPath().get("data[0].timestamp_utc"));
			Log.INFO("weather : " + weatherbitApi.getResponse().jsonPath().get("data[0].weather"));
		}
		
		
	}
	
	@Given("^I have a weather current weather api that can fetch weather for (.*?) and (.*?) using current weather api request$")
	public void addTestDataToCurrentWeatherAPIURI(String latitude, String longitude) {
		weatherbitApi = new RestAPI("current?key=f1cbf6c7744444eb9b5a890801ec4ee8&lat=" + latitude +"&lon=" + longitude + "&lang=en");
	}

}