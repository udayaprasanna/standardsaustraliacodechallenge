@nabui
Feature: Home loan enquiry tests

  Scenario Outline: Home loan enquiry
    Given I am at the NAB home page
    When I enquire about <HomeLoanType> for <EnquiryType>
    And I enter my <FirstName>, <LastName>, <State>, <PhoneNumber>, <Email> in the call back form and click submit
    Then the callback request should be accepted and thank you page with <FirstName> should be displayed 

    Examples: 
      | HomeLoanType   | EnquiryType           | FirstName | LastName | State | PhoneNumber | Email         |
      | New home loans | Buying a new property | Paul      | Stirling | NSW   |  0458974521 | rty@super.com |
