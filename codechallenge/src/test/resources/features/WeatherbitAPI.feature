@weatherbitapi
Feature: Weatherbit api tests

  @current
  Scenario Outline: Weatherbit current weather api test
    Given I have a weather current weather api that can fetch weather for <lat> and <lon> using current weather api request
    When I send the request to current weather API
    Then the the reponse validation for <ExpectedResponseCode> should pass

    Examples: 
      | lat       | lon        | ExpectedResponseCode |
      | 40.730610 | -73.935242 |                  200 |

  @forecast
  Scenario Outline: Weatherbit weather forecast api test
    Given I have a weather forecast api that can fetch weather for <PostalCode> using weather forecast api request
    When I send the request to weather forecast API
    Then the the reponse validation for <ExpectedResponseCode> should pass

    Examples: 
      | PostalCode | ExpectedResponseCode |
      |      02101 |                  200 |
